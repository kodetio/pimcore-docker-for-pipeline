FROM php:7.4-apache
RUN docker-php-ext-install pdo_mysql mysqli \
         && a2enmod rewrite \
		 && a2enmod headers \
		&& rm -rf /var/lib/apt/lists/*
RUN apt-get update \
	&& apt-get install gcc make autoconf libc-dev pkg-config -y --no-install-recommends --assume-yes --quiet ca-certificates curl git wget unzip  \
        libfreetype6-dev \
		libjpeg62-turbo-dev \
		libmcrypt-dev \
		libpng-dev \
        libfreetype6-dev \
        zlib1g-dev \
        libicu-dev \
        libxml2-dev \
        g++ \
        libonig-dev \
        libzip-dev
RUN docker-php-ext-install calendar
RUN docker-php-ext-enable calendar
RUN docker-php-ext-install mbstring
RUN docker-php-ext-install gd
RUN docker-php-ext-enable gd
RUN docker-php-ext-install intl
RUN docker-php-ext-enable intl
RUN docker-php-ext-install exif
RUN docker-php-ext-enable exif
RUN docker-php-ext-install zip
RUN docker-php-ext-enable zip
RUN docker-php-ext-install opcache
RUN docker-php-ext-enable opcache
# increase memory limit
RUN echo 'memory_limit = 1024M' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini;
# increase time limit
RUN echo 'max_execution_time = 3000' >> /usr/local/etc/php/conf.d/docker-php-times.ini;
RUN echo 'max_input_time = 3000' >> /usr/local/etc/php/conf.d/docker-php-times.ini;
# Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer
